package util;

import components.Spritesheet;
import render.Shader;
import render.Texture;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AssetPool {
  private static Map<String, Shader> shaders = new HashMap<>();
  private static Map<String, Texture> textures = new HashMap<>();
  private static Map<String, Spritesheet> spritesheets = new HashMap<>();

  public static Shader getShader(String resourceName) {
    File file = new File(resourceName);
    if (shaders.containsKey(file.getAbsolutePath())) {
      return shaders.get(file.getAbsolutePath());
    }
    Shader shader = new Shader(resourceName);
    shader.compile();
    AssetPool.shaders.put(file.getAbsolutePath(), shader);
     return shader;
  }

  public static Texture getTexture(String resourceName) {
    File file = new File(resourceName);
    if (textures.containsKey(file.getAbsolutePath())) {
      return textures.get(file.getAbsolutePath());
    }
    Texture texture = new Texture(resourceName);
     AssetPool.textures.put(file.getAbsolutePath(), texture);
     return texture;
  }

  public static void addSpritesheet(String recourceName, Spritesheet spritesheet){
    File file = new File(recourceName);
    if (!AssetPool.spritesheets.containsKey(file.getAbsolutePath())){
      AssetPool.spritesheets.put(file.getAbsolutePath(), spritesheet);
    }
  }

  public static Spritesheet getSpritesheet(String recourceName){
    File file = new File(recourceName);
    if (!AssetPool.spritesheets.containsKey(file.getAbsolutePath())){
      assert  false : "Error: Tried to acces spritesheet: " + recourceName + " and it has not been added to assetpool.";
    }
    return  AssetPool.spritesheets.getOrDefault(file.getAbsolutePath(),null);
  }
}
