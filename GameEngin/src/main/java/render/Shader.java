package render;

import org.joml.*;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;

public class Shader {

  private int shaderProgramID;
  private boolean beingUsed = false;
  private String vertexSource;
  private String fragmentSource;
  private String filePath;

  public Shader(String filePath) {
    this.filePath = filePath;
    try {
      String source = new String(Files.readAllBytes(Paths.get(filePath)));
      String[] splitString = source.split(("(#type)( )+([a-zA-Z])+")); // todo check the regex

      // find the first pattern after #type 'pattern'
      int index = source.indexOf("#type") + 6;
      int endOfLine = source.indexOf("\r\n", index); // for Linux only \n
      String firstPattern = source.substring(index, endOfLine).trim();

      // find the second pattern after #type 'pattern'
      index = source.indexOf("#type", endOfLine) + 6;
      endOfLine = source.indexOf("\r\n", index);
      String secondPattern = source.substring(index, endOfLine).trim();

      validatePattern(firstPattern, splitString[1]);
      validatePattern(secondPattern, splitString[2]);

    } catch (IOException e) {
      e.printStackTrace(); // todo to think of logging everything in log file
      assert false : "Error Could not open file ' " + filePath + "'";
    }
  }

  public void compile() {
    int vertexID;
    int fragmentID;
    // =====================================
    // Compile and link shaders
    // =====================================

    // first load and compile the vertex shader
    vertexID = glCreateShader(GL_VERTEX_SHADER);
    // pass the shader source to the GPU
    glShaderSource(vertexID, vertexSource);
    glCompileShader(vertexID);

    // Check for errors in compilation
    int success = glGetShaderi(vertexID, GL_COMPILE_STATUS);
    if (success == GL_FALSE) {
      int len = glGetShaderi(vertexID, GL_INFO_LOG_LENGTH);
      System.out.println("ERROR: '" + filePath + "'\n\tVertex shader compilation failed.");
      System.out.println(
          glGetShaderInfoLog(
              vertexID, len)); // the len variable probably is redundant(must think about it
      assert false
          : ""; // you need to enable assertions for this(I think you have to run the program with
      // -ea)
    }

    // first load and compile the fragment shader
    fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    // pass the shader source to the GPU
    glShaderSource(fragmentID, fragmentSource);
    glCompileShader(fragmentID);

    // Check for errors in compilation
    success = glGetShaderi(fragmentID, GL_COMPILE_STATUS);
    if (success == GL_FALSE) {
      int len = glGetShaderi(fragmentID, GL_INFO_LOG_LENGTH);
      System.out.println("ERROR: '" + filePath + "'\n\tFragment shader compilation failed.");
      System.out.println(
          glGetShaderInfoLog(
              fragmentID, len)); // the len variable probably is redundant(must think about it
      assert false
          : ""; // you need to enable assertions for this(I think you have to run the program with
      // -ea)
    }
    // Link shaders and check for errors
    shaderProgramID = glCreateProgram();
    glAttachShader(shaderProgramID, vertexID);
    glAttachShader(shaderProgramID, fragmentID);
    glLinkProgram(shaderProgramID);

    // Check for linking errors
    success = glGetProgrami(shaderProgramID, GL_LINK_STATUS);
    if (success == GL_FALSE) {
      int len = glGetProgrami(shaderProgramID, GL_INFO_LOG_LENGTH);
      System.out.println("ERROR: '" + filePath + "'\n\tLinking of shaders failed.");
      System.out.println(
          glGetProgramInfoLog(
              shaderProgramID, len)); // the len variable probably is redundant(must think about it
      assert false
          : ""; // you need to enable assertions for this(I think you have to run the program with
      // -ea)
    }
  }

  public void use() {
    if (!beingUsed) {
      // Bind shader program
      glUseProgram(shaderProgramID);
      beingUsed = true;
    }
  }

  public void detach() {
    glUseProgram(0);
    beingUsed = false;
  }

  public void uploadMat4f(String variableName, Matrix4f matrix4){
    int variableLocation = glGetUniformLocation(shaderProgramID,variableName);
    use();//to be sure that  we are  using the shader.
    FloatBuffer matBuffer = BufferUtils.createFloatBuffer(16);
    matrix4.get(matBuffer);
    glUniformMatrix4fv(variableLocation,false,matBuffer);
  }

  public void uploadMat3f(String variableName, Matrix3f matrix3){
    int variableLocation = glGetUniformLocation(shaderProgramID,variableName);
    use();//to be sure that  we are  using the shader.
    FloatBuffer matBuffer = BufferUtils.createFloatBuffer(9);
    matrix3.get(matBuffer);
    glUniformMatrix3fv(variableLocation,false,matBuffer);
  }

  public void uploadVec4f(String varName, Vector4f vector){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform4f(varLocation,vector.x,vector.y,vector.z,vector.w);
  }

  public void uploadVec3f(String varName, Vector3f vector){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform3f(varLocation,vector.x,vector.y,vector.z);
  }

  public void uploadVec2f(String varName, Vector2f vector){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform2f(varLocation,vector.x,vector.y);
  }

  public void uploadFloat(String varName, float value){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform1f(varLocation,value);
  }
  public  void uploadIn(String varName, int value){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform1i(varLocation,value);
  }

  public  void uploadTexture(String varName, int slot){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform1i(varLocation,slot);
  }

  public  void uploadIntArray(String varName, int[] array){
    int varLocation = glGetUniformLocation(shaderProgramID,varName);
    use();
    glUniform1iv(varLocation,array);
  }

  private void validatePattern(String pattern, String splitString)throws IOException{
    if (pattern.equalsIgnoreCase("vertex")) {
      vertexSource = splitString;
    }
    if (pattern.equalsIgnoreCase("fragment")) {
      fragmentSource = splitString;
    }
    if (!pattern.equalsIgnoreCase("vertex") && !pattern.equalsIgnoreCase("fragment")){
      throw new IOException("Unexpected token: " + pattern);
    }
  }
}
