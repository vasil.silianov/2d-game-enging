package render;

import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.*;

public class Texture {
  private String filePath;
  private int textID;
  private int width;
  private int height;


  public Texture(String filePath) {
    this.filePath = filePath;

    // todo I do not like thinks like this in the constructor, must  define a method( setMethod or better init() method)  to
    // take the out if the constructor
    // Generate Texture on the GPU
    textID = glGenTextures();
    glBindTexture(GL_TEXTURE_2D, textID);

    // Set texture parameters
    // Repeat image in both directions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // When stretching the  image, pixalete
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // When SHRINKING the  image, pixalete
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // LOAD THE IMAGE
    IntBuffer width = BufferUtils.createIntBuffer(1);
    IntBuffer height = BufferUtils.createIntBuffer(1);
    IntBuffer channels = BufferUtils.createIntBuffer(1);
    stbi_set_flip_vertically_on_load(true);
    ByteBuffer image = stbi_load(filePath, width, height, channels, 0);

    if (image != null) {
      this.width = width.get(0);
      this.height = height.get(0);
      // we actually upload the texture to the GPU
      if (channels.get(0) == 3){
        glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RGB,
                width.get(0),
                height.get(0),
                0,
                GL_RGB,
                GL_UNSIGNED_BYTE,
                image);
      } else if (channels.get(0) == 4){

        glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RGBA,
                width.get(0),
                height.get(0),
                0,
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                image);
      } else {
        assert  false : "Error: (Texture) Unknown number of channels: " + channels.get(0);
      }
    } else {
      assert false : " Error: Texture file, could not  load image: " + filePath;
    }
    // free's the memory that stbi has allocated for the image
    stbi_image_free(image);
  }

  public void bind() {
    glBindTexture(GL_TEXTURE_2D, textID);
  }

  public void unBind() {
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }
}
