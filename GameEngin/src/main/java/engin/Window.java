package engin;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import util.Time;

import java.util.Objects;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window {
  private final int width;
  private final int height;
  private final String title;
  public float r, g, b, a;
  private boolean fadeToBlack = false;
  private static Window window = null;
  private long glfwWindow;
  private static Scene currentScene;

  private Window() {
    this.width = 1920;
    this.height = 1080;
    this.title = "Game name";
    r = 0;
    b = 0;
    g = 0;
    a = 1;
  }

  public static void changeScene(int newScene) {
    switch (newScene) {
      case 0:
        currentScene = new LevelEditorScene();
        currentScene.init();
        currentScene.start();
        break;
      case 1:
        currentScene = new LevelScene();
        currentScene.init();
        currentScene.start();
        break;
      default:
        assert false : "Unknown scene '" + newScene + "'";
        break;
    }
  }

  public static Window get() {
    if (Window.window == null) {
      Window.window = new Window();
    }
    return window;
  }

  public static Scene getScene() {
    return get().currentScene;
  }
  /** For reference This method is basically copy past version from: https://www.lwjgl.org/guide */
  public void run() {
    System.out.println(
        "Hello Vasko " + Version.getVersion() + "!"); // TODO  to replace Vasko with LWJGL
    init();
    loop();

    // Free the memory
    glfwFreeCallbacks(glfwWindow);
    glfwDestroyWindow(glfwWindow);

    // Terminate GLFW and  the free the error callback
    glfwTerminate();
    Objects.requireNonNull(glfwSetErrorCallback(null)).free();
  }

  public void init() {
    // Setup an error callback
    GLFWErrorCallback.createPrint(System.err).set();

    // Initialize GLFW
    if (!glfwInit()) {
      throw new IllegalStateException("Unable to initialize GLFW.");
    }

    // CONFIGURE GLFW
    glfwDefaultWindowHints();
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);

    // Create Window
    glfwWindow = glfwCreateWindow(this.width, this.height, this.title, NULL, NULL);
    if (glfwWindow == NULL) {
      throw new IllegalStateException("Failed to create the GLFW window.");
    }

    glfwSetCursorPosCallback(glfwWindow, MouseListener::mousePosCallback); // 0:43 minutes
    glfwSetMouseButtonCallback(glfwWindow, MouseListener::mouseButtonCallback);
    glfwSetScrollCallback(glfwWindow, MouseListener::mouseScrollCallback);
    glfwSetKeyCallback(glfwWindow, KeyListener::keyCallback); // 0:47 minutes

    // make the OpenGL context current
    glfwMakeContextCurrent(glfwWindow);
    // enable v-sync
    glfwSwapInterval(1);

    // Make the window visible
    glfwShowWindow(glfwWindow);
    // This line is critical for LWJGL's interoperation with GLFW's
    // OpenGL context, or any context that is managed externally.
    // LWJGL detects the context that is current in the current thread,
    // creates the GLCapabilities instance and makes the OpenGL
    // bindings available for use.
    GL.createCapabilities();
    Window.changeScene(0);
  }

  public void loop() {
    float beginTime = (float)glfwGetTime();
    float endTime;
    float dt = -1.0f;
    while (!glfwWindowShouldClose(glfwWindow)) {
      // poll events
      glfwPollEvents();

      glClearColor(r, g, b, a);
      glClear(GL_COLOR_BUFFER_BIT);

      if (dt >= 0) {
        currentScene.update(dt);
      }
      //            //testing the  listeners
      //            if (KeyListener.isKeyPressed(GLFW_KEY_SPACE)) {
      //                System.out.println("we are pressing SPACE");
      //            }
      //            if (KeyListener.isKeyPressed(GLFW_KEY_A)) {
      //                System.out.println("we are pressing A");
      //            }
      //            if (KeyListener.isKeyPressed(GLFW_KEY_7)) {
      //                System.out.println("we are pressing 7");
      //            }
      //            if (KeyListener.isKeyPressed(GLFW_KEY_L)) {
      //                System.out.println("we are pressing L");
      //            }
      //            if (MouseListener.mouseButtonDown(GLFW_MOUSE_BUTTON_1)) {
      //                System.out.println("we are pressing mouse button 1");
      //            }
      //            if (MouseListener.mouseButtonDown(GLFW_MOUSE_BUTTON_2)) {
      //                System.out.println("we are pressing mouse button 2");
      //            }

      glfwSwapBuffers(glfwWindow);
      endTime = (float)glfwGetTime();

      dt = endTime - beginTime;
      beginTime = endTime;
    }
  }
}
