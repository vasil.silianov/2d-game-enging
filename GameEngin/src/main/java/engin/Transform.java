package engin;

import org.joml.Vector2f;

public class Transform {
  public Vector2f position;
  public Vector2f scale;

  public Transform() {
    init(new Vector2f(), new Vector2f());
  }

  public Transform(Vector2f position) {
    init(position, new Vector2f());
  }

  public Transform(Vector2f position, Vector2f scale) {
    init(position, scale);
  }

  public void init(
      Vector2f position,
      Vector2f
          scale) { // why do we do an init method and call it into the constructor instead of  using
    // the constructor directly, we will create object easlly but we have to
    // initialize them again
    this.position = position;
    this.scale = scale;
  }
}
