package engin;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class MouseListener {

  private static MouseListener instance;
  private double scrollX;
  private double scrollY;
  private double xPos;
  private double yPos;
  private double lastX;
  private double lastY;
  private boolean[] mouseButtonPressed = new boolean[3]; // Could be more than three buttons pressed
  private boolean isDragging;

  private MouseListener() { // We  want this to be singleton that is why it is private

    this.scrollX = 0.0;
    this.scrollY = 0.0;
    this.xPos = 0.0;
    this.yPos = 0.0;
    this.lastX = 0.0;
    this.lastY = 0.0;
  }

  public static MouseListener get() { // We initialize the instance
    if (MouseListener.instance == null) {
      MouseListener.instance = new MouseListener();
    }
    return MouseListener.instance;
  }

  public static void mousePosCallback(
      long window,
      double xpos,
      double
          ypos) { // todo think of better naming for "double xpos,double ypos" variables  // may be
                  // newXpos and newYpos to think about
    get().lastX = get().xPos;
    get().lastY = get().yPos;
    get().xPos = xpos;
    get().yPos = ypos;
    //        get().isDragging = get().mouseButtonPressed[0] || get().mouseButtonPressed[1] ||
    // get().mouseButtonPressed[2]; // this is how we check are we dragging the mouse( this is the
    // original version, but  I do not like it so my version is  below)

    for (Boolean button :
        get()
            .mouseButtonPressed) { // thus way I can change the number of buttons without  adding
                                   // new  conditions. //TODO  review it carefully
      if (button == true) {
        get().isDragging = true;
        break;
      }
    }
  }

  public static void mouseButtonCallback(long window, int button, int action, int mods) {
    if (action == GLFW_PRESS) {
      if (button < get().mouseButtonPressed.length) {
        get().mouseButtonPressed[button] =
            true; // We put  true in this position of the array(mouseButtonPressed) to know that
                  // this button is pressed
      }
    }
    // here i've done it a little bit different( around 0:35 minutes)
    if (action == GLFW_RELEASE) {
      if (button < get().mouseButtonPressed.length) {
        get().mouseButtonPressed[button] =
            false; // We put  false in this position of the array(mouseButtonPressed) to know that
                   // this button is released
        get().isDragging = false;
      }
    }
  }

  public static void mouseScrollCallback(
      long window, double xOffset, double yOffset) { // around 0:37 minutes
    get().scrollX = xOffset;
    get().scrollY = yOffset;
  }

  // getters
  public static float getX() {
    return (float) get().xPos; // why do we need  to cast the xPos and yPos, why do we need floats
  }

  public static float getY() {
    return (float) get().yPos;
  }

  public static float getDx() {
    return (float)
        (get().lastX
            - get()
                .xPos); // the difference  between  starting position and current position, should
                        // think about better naming
  }

  public static float getDy() {
    return (float) (get().lastY - get().yPos);
  }

  public static float getScrollX() {
    return (float) get().scrollX;
  }

  public static float getScrollY() {
    return (float) get().scrollY;
  }

  public static boolean mouseButtonDown(int button) {
    if (button < get().mouseButtonPressed.length) {
      return get().mouseButtonPressed[button];
    }
    return false;
  }
}
