package engin;

import java.util.ArrayList;
import java.util.List;

public class GameObject {
  private String name;
  private List<Component> components;
  public Transform transform;

  public GameObject(String name) {
    this.name = name;
    this.components = new ArrayList<>();
    this.transform = new Transform();
  }

  public GameObject(String name, Transform transform){ // todo he suggests for us to create init function
    this.name = name;
    this.components = new ArrayList<>();
    this.transform = transform;
  }

  public <T extends Component> T getComponent(Class<T> componentClass) {
    for (Component component : components) {
      if (componentClass.isAssignableFrom(component.getClass())) {
        try {
          return componentClass.cast(component);
        } catch (ClassCastException exception) {
          exception.printStackTrace();
          assert false : "Error: Unable to cast component: " + component.getClass();
        }
      }
    }
    return null; // todo should think of something to try to return of nullable
  }

  public <T extends Component> void removeComponent(Class<T> componentClass) {
    for (int i = 0; i < components.size(); i++) {
      Component component = components.get(i);
      if (componentClass.isAssignableFrom(component.getClass())) {
        components.remove(i);
        return;
      }
    }
  }

  public void  addComponent(Component component){
    this.components.add(component);
    component.gameObject = this; // we assign  the reference to component's game object, all components will have reverence to their  parent which  is the GameObject
  }

  public void update(float dt){
    for(int i = 0; i <components.size() ; i++) {
      components.get(i).update(dt);
    }
  }

  public void start(){
    for(int i = 0; i < components.size(); i++) {
      components.get(i).start();
    }
  }
}
