package engin;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class KeyListener {

  private static KeyListener instance;
  private boolean[] keyPressed =
      new boolean[350]; // must check documentation why it is 350, is it the max limit of buttons;

  private KeyListener() { // We want it to be a singleton
  }

  private static KeyListener get() {
    if (KeyListener.instance == null) {
      KeyListener.instance = new KeyListener();
    }
    return KeyListener.instance;
  }

  public static void keyCallback(long window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
      get().keyPressed[key] = true;
    }

    if (action == GLFW_RELEASE) {
      get().keyPressed[key] = false;
    }
  }

  public static boolean isKeyPressed(int keyCode) {
    return get().keyPressed[keyCode];
  }
}
