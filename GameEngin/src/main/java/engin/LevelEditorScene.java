package engin;

import components.Sprite;
import components.SpriteRenderer;
import components.Spritesheet;
import org.joml.Vector2f;
import org.joml.Vector4f;
import util.AssetPool;

import static constants.assets_constants.image_constants.ImageConstants.*;
import static constants.assets_constants.shader_constants.ShaderConstants.SHADER_PATH;

public class LevelEditorScene extends Scene {

  public LevelEditorScene() {}

  @Override
  public void init() {
    loadResources();
    this.camera = new Camera(new Vector2f());

    Spritesheet sprites = AssetPool.getSpritesheet(SPRITESHEET_PATH);


    GameObject objectVasko = new GameObject("Vasko", new Transform(new Vector2f(100, 100), new Vector2f(50,50)));
    objectVasko.addComponent(new SpriteRenderer(sprites.getSprite(0)));
    this.addGameObjectToScene(objectVasko);

    GameObject objectIrnik = new GameObject("Irnik", new Transform(new Vector2f(400, 100), new Vector2f(256,256)));
    objectIrnik.addComponent(new SpriteRenderer(sprites.getSprite(10)));
    this.addGameObjectToScene(objectIrnik);


//    int xOffset = 10;
//    int yOffset = 10;

//    float totalWidth =  (float) (600 - xOffset * 2);
//    float totalHeight =  (float) (300 - yOffset * 2);

//    float sizeX = totalWidth / 100.0f;
//    float sizeY = totalWidth / 100.0f;

//    for(int x = 0; x < 100; x++) {
//      for(int y = 0; y < 100; y++) {
//        float xPos = xOffset + (x * sizeX);
//        float yPos = xOffset + (y * sizeX);
//        GameObject gameObject = new GameObject("Object" + x + " " + y,new Transform(new Vector2f(xPos,yPos), new Vector2f(sizeX,sizeY)));
//        gameObject.addComponent(new SpriteRenderer(new Vector4f(xPos/totalWidth,yPos/totalHeight,1,1)));
//        this.addGameObjectToScene(gameObject);
//      }
//    }

  }

  private void loadResources(){
    AssetPool.getShader(SHADER_PATH);
    AssetPool.addSpritesheet(SPRITESHEET_PATH,
            new Spritesheet(AssetPool.getTexture(SPRITESHEET_PATH),
                    16,16,26,0));

  }

  @Override
  public void update(float dt) {
    System.out.println("FPS: " + (1.0f / dt));

    for (GameObject gameObject : this.gameObjects){
      gameObject.update(dt);
    }

    this.renderer.render();
  }
}
