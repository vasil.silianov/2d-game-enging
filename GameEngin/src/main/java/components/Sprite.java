package components;

import org.joml.Vector2f;
import render.Texture;

public class Sprite {


    private Vector2f[] textureCoordinates;
    private Texture texture;

    public Sprite(Texture texture) {
        this.texture = texture;
        setTextureCoordinates();// Is this  working correctly
    }

    public Sprite(Texture texture, Vector2f[] textureCoordinates) {
        this.textureCoordinates = textureCoordinates;
        this.texture = texture;
    }

    public Vector2f[] getTextureCoordinates() {
        return textureCoordinates;
    }

    public Texture getTexture() {
        return texture;
    }

    private void setTextureCoordinates() {
        Vector2f[] textureCoordinates =
                new Vector2f[]{
                        new Vector2f(1,1),
                        new Vector2f(1,0),
                        new Vector2f(0,0),
                        new Vector2f(0,1)
                };

        this.textureCoordinates = textureCoordinates;
    }
}
