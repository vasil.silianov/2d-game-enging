package constants.assets_constants.image_constants;

public class ImageConstants {
    public static final String TEXTURE_PATH = "assets/images/testImage.jpg";
    public static final String MARIO_PATH = "assets/images/mario.png";
    public static final String VASKO_PATH = "assets/images/testImage1.png";
    public static final String IRNIK_PATH = "assets/images/testImage2.png";
    public static final String SPRITESHEET_PATH = "assets/images/spritesheet.png";
}
