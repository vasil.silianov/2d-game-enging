#type vertex
#version 330 core
layout (location=0) in vec3 aPos;
layout (location=1) in vec4 aColor;
layout (location=2) in vec2 aTextureCoordinates;
layout (location=3) in float aTextureId;


uniform mat4 uProjection;
uniform mat4 uView;

out vec4 fColor;
out vec2 fTextureCoordinates;
out float fTextureId;

void main(){
    fColor = aColor;
    fTextureCoordinates = aTextureCoordinates;
    fTextureId = aTextureId;
    gl_Position = uProjection * uView * vec4(aPos, 1.0);
}

#type fragment
#version 330 core


in vec4 fColor;
in vec2 fTextureCoordinates;
in float fTextureId;

uniform sampler2D uTextures[8];

out vec4 color;

void main(){

    if (fTextureId > 0 ){
        int id = int(fTextureId);
        color =  fColor * texture(uTextures[id], fTextureCoordinates);
//    color = vec4(fTextureCoordinates,0,1);
    } else {// here the else is important

        color =  fColor;
    }

    //#3
//    float noise = fract(sin(dot(fColor.xy , vec2(12.9898,78.233))) * 43758.5453);
//    color = fColor * noise;
    //#2
//    float avg = (fColor.r + fColor.g + fColor.b) / 3;
//    color = vec4(avg,avg,avg,1);
    //#1
//    color = sin(uTime) * fColor;
}
